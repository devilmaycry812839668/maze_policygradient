# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 13:06:40 2020

@author: 81283
"""

import numpy as np

# _theta_0 agent在环境状态中的动作集合：上，右，下，左
theta_0 = np.array([[np.nan, 1, 1, np.nan],  # s0
                    [np.nan, 1, np.nan, 1],  # s1
                    [np.nan, np.nan, 1, 1],  # s2
                    [1, 1, 1, np.nan],  # s3
                    [np.nan, np.nan, 1, 1],  # s4
                    [1, np.nan, np.nan, np.nan],  # s5
                    [1, np.nan, np.nan, np.nan],  # s6
                    [1, 1, np.nan, np.nan],  # s7、※s8为终止状态（无动作）
                    ])

